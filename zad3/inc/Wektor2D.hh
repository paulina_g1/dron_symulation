#ifndef WEKTOR2D_HH
#define WEKTOR2D_HH

#include <iostream>
#include <array>
#include <cmath>

/*modeluje pojecie wektora w przestrzeni dwuwymiarowej*/

class Wektor2D {
  std::array<double,2> wsp;
  public:
  Wektor2D operator+(const Wektor2D & skl2) const;
  Wektor2D operator-(const Wektor2D & skl2) const;
  Wektor2D operator*(double skl2) const;
  double operator*(const Wektor2D & skl2) const; //skalarne
  double dlugosc() const;
  // konstruktory:
  Wektor2D(double x, double y) {wsp[0]=x; wsp[1]=y;};
  Wektor2D() {wsp[0]=0.0; wsp[1]=0.0;};
  //get:
  const double & operator[] (int i) const{
    if(i<0||i>1){std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wsp[i];}
  //set:
  double & operator[](int i){
    if(i<0||i>1){std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wsp[i];}
};

std::istream& operator >> (std::istream &strm, Wektor2D &wek);
std::ostream& operator << (std::ostream &strm, const Wektor2D &wek);

#endif
