#include "Prostokat.hh"

/************************************************************
 * Obraca prostokat o zadany kat                            *
 * Argumenty:                                               *
 *      kat_stopnie - kat obrotu w stopniach				*
 ***********************************************************/
  void Prostokat::obrot(double kat_stopnie){
      Macierz2x2 mac_obr(kat_stopnie);
      wierzcholek[0]=mac_obr*wierzcholek[0];
      wierzcholek[1]=mac_obr*wierzcholek[1];
      wierzcholek[2]=mac_obr*wierzcholek[2];
      wierzcholek[3]=mac_obr*wierzcholek[3];
  }

/************************************************************
 * Przesuwa prostokat o zadany wektor                       *
 * Argumenty:                                               *
 *      W - wektor przesuniecia podany w formacie (1,2)		*
 ***********************************************************/
  void Prostokat::przesuniecie (Wektor2D W){
      wierzcholek[0]=wierzcholek[0]+W;
      wierzcholek[1]=wierzcholek[1]+W;
      wierzcholek[2]=wierzcholek[2]+W;
      wierzcholek[3]=wierzcholek[3]+W;
  }

/************************************************************
 * Porownuje przeciwlegle boki prostokata i sprawdza czy    *
 * katy sa proste                                           *
 * Zwraca:													*
 *    	true jesli ksztalt spelnia warunki prostokata,      *
 *      false jesli nie         							*
 ***********************************************************/
  bool Prostokat::czyProstokat(){
    const double EPSILON = 1E-14;
    Wektor2D bok_0_1 = wierzcholek[0]-wierzcholek[1];
    Wektor2D bok_1_2 = wierzcholek[1]-wierzcholek[2];
    Wektor2D bok_2_3 = wierzcholek[2]-wierzcholek[3];
    Wektor2D bok_3_0 = wierzcholek[3]-wierzcholek[0];

	if (fabs(bok_0_1.dlugosc()-bok_2_3.dlugosc()) > EPSILON) {
		return false;
	}
	if (fabs(bok_1_2.dlugosc()-bok_3_0.dlugosc()) > EPSILON) {
		return false;
	}
    if ((bok_0_1*bok_1_2)>EPSILON){
        return false;
    }
    if ((bok_1_2*bok_2_3)>EPSILON){
        return false;
    }
    if ((bok_2_3*bok_3_0)>EPSILON){
        return false;
    }
    if ((bok_3_0*bok_0_1)>EPSILON){
        return false;
    }
	return true;
  }

/************************************************************
 * Rysuje prostokat za pomoca odpowiedniego zlacza do gnuplota*
 * Argumenty:                                               *
 *      rysownik - wskaznik na element typu Draw2DAPI		*
 ***********************************************************/
  void Prostokat::rysuj(drawNS::Draw2DAPI* rysownik){
      std::vector<drawNS::Point2D> vtmp;
      vtmp.push_back(konwertuj(wierzcholek[0]));
      vtmp.push_back(konwertuj(wierzcholek[1]));
      vtmp.push_back(konwertuj(wierzcholek[2]));
      vtmp.push_back(konwertuj(wierzcholek[3]));
      vtmp.push_back(konwertuj(wierzcholek[0]));
      id = rysownik->draw_polygonal_chain(vtmp,"purple");
  }

/************************************************************
 * Zmazuje narysowany wczesniej prostokat                   *
 * Argumenty:                                               *
*      rysownik - wskaznik na element typu Draw2DAPI		*
 ***********************************************************/
    void Prostokat::usun_rys (drawNS::Draw2DAPI* rysownik){
        rysownik->erase_shape(id);
    }   

/************************************************************
 * kostruktor dla klasy Prostokat,                          *
 * sprawdza czy zadane wiercholki tworza prostokat          *
 * Argumenty:                                               *
*      LG - lewy gorny wierzcholek                          *
*      PG - prawy gorny wierzcholek                         *
*      PD - prawy dolny wierzcholek                         *
*      LD - lewy dolny wierzcholek                          *
 ***********************************************************/
  Prostokat::Prostokat (Wektor2D LG, Wektor2D PG, Wektor2D PD, Wektor2D LD){

          wierzcholek[0]=LG;
          wierzcholek[1]=PG;
          wierzcholek[2]=PD;
          wierzcholek[3]=LD;
        if(czyProstokat() == false){
          std::cerr << "Podane wierzcholki nie tworza prostokata!" << std::endl;
          exit(2);
      }
  }

/************************************************************
 * Wypisuje wierzcholki prostokata na strumien wyjsciowy	*
 * Argumenty:												*
 *    Strm - strumien										*
 *    PR - wyswietlany prostokat      						*
 * Zwraca:													*
 *    Strumien z zapisem wiercholkow w formie:              *
 *      (1,2)   (4,1)                              			*
 *      (1,0)   (4,0)                              			*
 ***********************************************************/
  std::ostream& operator << ( std::ostream &Strm,const Prostokat&Pr){
      Strm << Pr[0] << "    " << Pr[1] << std::endl;
      Strm << Pr[3] << "    " << Pr[2] << std::endl;
    return Strm;
  }


/************************************************************
 * Konwertuje wektor na Point2D                             *
 * Argumenty:                                               *
 *      W - wektor do przekonwertowania    					*
 * Zwraca:													*
 *    	Przekonwertowany punkt    							*
 ***********************************************************/
  drawNS::Point2D konwertuj(Wektor2D W){
      return drawNS::Point2D(W[0],W[1]);
}