#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
#include <iostream>
#include <cmath>

/****************************************************************
 *  Plik zawiera definicje klasy LZesplona oraz zapowiedzi		*
 *  przeciazen operatorow dzialajacych na tej                   *
 *  klasie.		                                                *
 ****************************************************************/

class  LZespolona {
private:
  double   re;    /* czesc rzeczywista */
  double   im;    /* czesc urojona */
public:
	double modul() const;
	LZespolona inicjuj_licz(double Re, double Im); 
	LZespolona sprzezenie() const;
	LZespolona  operator + (const LZespolona & Skl2) const;
	LZespolona  operator - (const LZespolona & Skl2) const;
	LZespolona  operator * (const LZespolona & Skl2) const;
	LZespolona  operator / (const LZespolona & Skl2) const;
	LZespolona  operator / (double Skl2) const;
	bool operator == (const LZespolona& Skl2) const;
	bool operator != (const LZespolona& Skl2) const;
	// konstruktory:
	LZespolona(double Re, double Im) : re(Re), im(Im) {} 
	explicit LZespolona(double Re) : re(Re), im(0.0) {}
	LZespolona() : re(0.0), im(0.0) {}
	//pobieranie i ustawianie wartości re i im:
	double get_re() const { return re; };
	double get_im() const { return im; };
	void set_re(double Re) { re = Re; };
	void set_im(double Im) { im = Im; };

};

std::ostream& operator << (std::ostream& s, const LZespolona& Liczba);
std::istream& operator >> (std::istream& s, LZespolona& Liczba);

#endif
