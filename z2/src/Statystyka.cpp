#include <iostream>
#include <cmath>
#include "Statystyka.hh"

/************************************************************
 * Metoda inicjuje statystyke testu 						*
 * Argumenty:												*
 *    Liczba_popraw - liczba poprawnych odpowiedzi 		    *
 *    Liczba_niepopraw - liczba niepoprawnych odpowiedzi    *
 * Zwraca:													*
 *    zainicjowana statystyke testu 						*
 ***********************************************************/
Statystyki Statystyki::inicjuj_stat(unsigned int Liczba_popraw, unsigned int Liczba_niepopraw){
    Statystyki stat;
    stat.L_poprawnych = Liczba_popraw;
    stat.L_niepoprawnych = Liczba_niepopraw;
    return stat;
}

/********************************************************************
 * Metoda oblicza procent poprawnych odpowiedzi			            *
 * Zwraca:													        *
 *    Procent poprawnych odpowiedzi                			        *
 *******************************************************************/
unsigned int Statystyki::oblicz_procent() const { 
    double ulamek = static_cast<double>(L_poprawnych)/static_cast<double>(L_poprawnych+L_niepoprawnych);
    return round(ulamek*100);
}

/********************************************************************
 * Metoda wyswietla liczbe poprawnych i niepoprawnych odp           *
 * oraz procent poprawnych odp                  	    	    	*
 *******************************************************************/
void Statystyki::wypisz_wynik() const {
    std::cout << "Liczba poprawnych odpowiedzi: " << L_poprawnych << std::endl;
    std::cout << "Liczba niepoprawnych odpowiedzi: " << L_niepoprawnych << std::endl;
    std::cout << "Wynik procentowo: " << oblicz_procent() << "%" << std::endl;
}

/*****************************************************************
 * Metoda dodaje informacje o dobrej odp do objektu klasy        *
 ****************************************************************/
void Statystyki::dodaj_poprawna(){
    L_poprawnych++;
}
/*****************************************************************
 * Mwtoda dodaje informacje o zlej odp do objektu klasy          *
 ****************************************************************/
void Statystyki::dodaj_niepoprawna(){
    L_niepoprawnych++;
}