#include "WyrazenieZesp.hh"

/************************************************************
 * Metoda inicjuje wyrazenie zespolone						*
 * Argumenty:												*
 *    Liczba1 - pierwszy argument wyrazenia     		    *
 *    Liczba2 - drugi argument wyrazenia         		    *
 * 	  O - operator (znak) dzialania     					*
 * Zwraca:													*
 *    zainicjowana wyrazenie zespolone						*
 ***********************************************************/
WyrazenieZesp WyrazenieZesp::inicjuj_wyr (const LZespolona & Liczba1,const LZespolona & Liczba2, Operator O){
    WyrazenieZesp Wyr;
    Wyr.Arg1 = Liczba1;
    Wyr.Arg2 = Liczba2;
    Wyr.Op = O;
    return Wyr;
}

/************************************************************
 * Metoda wyswietla wyrazenie zespolone    	    			*
 ***********************************************************/
void WyrazenieZesp::wyswietl() const{
    std::cout << *this;
}

/************************************************************
 * Metoda oblicza wyrazenie zespolone         				*
 * Zwraca:													*
 *    Wynik dzialania                                       *
 ***********************************************************/
LZespolona WyrazenieZesp::oblicz() const {
    LZespolona Wynik;
    switch (get_Op()){
        case Op_Dodaj:
            Wynik = get_Arg1() + get_Arg2(); break;
        case Op_Odejmij:
            Wynik = get_Arg1() - get_Arg2(); break;
        case Op_Mnoz:
            Wynik = get_Arg1() * get_Arg2(); break;
        case Op_Dziel:
            Wynik = get_Arg1() / get_Arg2(); break;
    }
    return Wynik;
}

/************************************************************
 * Wypisuje wyrazenie zespolone na strumien wyjsciowy		*
 * Argumenty:												*
 *    s - strumien											*
 *    Wyr - wyswietlane wyrazenie							*
 * Zwraca:													*
 *    Strumien z wypisanym wyrazeniem zespolonym   			*
 ***********************************************************/
std::ostream & operator << (std::ostream & s, const WyrazenieZesp & Wyr){
    s << Wyr.get_Arg1() << Wyr.get_Op() << Wyr.get_Arg2();
    return s;
}

/************************************************************
 * Wypisuje operator na strumien wyjsciowy	            	*
 * Argumenty:												*
 *    s - strumien											*
 *    O - wyswietlany operator      						*
 * Zwraca:													*
 *    Strumien z wypisanym operatorem             			*
 ***********************************************************/
std::ostream & operator << (std::ostream & s, const Operator & O){
    switch (O){
        case Op_Dodaj:
            s << "+"; break;
        case Op_Odejmij:
            s << "-"; break;
        case Op_Mnoz:
            s << "*"; break;
        case Op_Dziel:
            s << "/"; break;

    }
    return s;
}

/************************************************************
 * Wczytuje wyraznie zespolone ze strumienia wejsciowego	*
 * Argumenty:												*
 *    s - strumien											*
 *    Wyr - wczytywane wyrazenie							*
 * Zwraca:													*
 *    Strumien z wczytanym wyrazeniem           			*
 ***********************************************************/
std::istream & operator >> (std::istream & s,  WyrazenieZesp & Wyr){
    LZespolona temp;
    Operator temp_op;
    s >> temp;
    Wyr.set_Arg1(temp);
    s >> temp_op;
    Wyr.set_Op(temp_op);
    s >> temp;
    Wyr.set_Arg2(temp);
    return s;

}

/************************************************************
 * Wczytuje operator ze strumienia wejsciowego            	*
 * Argumenty:												*
 *    s - strumien											*
 *    O - wczytywany operator      			    			*
 * Zwraca:													*
 *    Strumien z wczytanym operatorem             			*
 ***********************************************************/
std::istream & operator >> (std::istream & s,  Operator & O){
    char znak;
    s >> znak;
        switch (znak){
        case '+':
            O = Op_Dodaj; break;
        case '-':
            O = Op_Odejmij; break;
        case '*':
            O = Op_Mnoz; break;
        case '/':
            O = Op_Dziel; break;
        default: 
            s.setstate(std::ios::failbit);
    }
    return s;
}
