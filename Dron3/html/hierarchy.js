var hierarchy =
[
    [ "drawNS::Draw3DAPI", "classdraw_n_s_1_1_draw3_d_a_p_i.html", [
      [ "drawNS::APIGnuPlot3D", "classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html", null ]
    ] ],
    [ "Interfejs_Dron", "class_interfejs___dron.html", [
      [ "Dron", "class_dron.html", null ],
      [ "Dron2", "class_dron2.html", null ],
      [ "Dron3", "class_dron3.html", null ]
    ] ],
    [ "Interfejs_elem_kraj", "class_interfejs__elem__kraj.html", [
      [ "Dron", "class_dron.html", null ],
      [ "Dron2", "class_dron2.html", null ],
      [ "Dron3", "class_dron3.html", null ],
      [ "Plaskowyz", "class_plaskowyz.html", null ],
      [ "Plaskowyz_prost", "class_plaskowyz__prost.html", null ],
      [ "Wzgorze", "class_wzgorze.html", null ]
    ] ],
    [ "Interfejs_rysowanie", "class_interfejs__rysowanie.html", [
      [ "Dron", "class_dron.html", null ],
      [ "Dron2", "class_dron2.html", null ],
      [ "Dron3", "class_dron3.html", null ],
      [ "Graniastoslup6", "class_graniastoslup6.html", null ],
      [ "Plaskowyz", "class_plaskowyz.html", null ],
      [ "Powierzchnia", "class_powierzchnia.html", null ],
      [ "Prostopadloscian", "class_prostopadloscian.html", [
        [ "Plaskowyz_prost", "class_plaskowyz__prost.html", null ]
      ] ],
      [ "Wzgorze", "class_wzgorze.html", null ]
    ] ],
    [ "Macierz_Rot< ROZMIAR >", "class_macierz___rot.html", null ],
    [ "Macierz_Rot< 3 >", "class_macierz___rot.html", null ],
    [ "drawNS::Point3D", "classdraw_n_s_1_1_point3_d.html", null ],
    [ "Scena", "class_scena.html", null ],
    [ "Uklad_W", "class_uklad___w.html", [
      [ "Dron", "class_dron.html", null ],
      [ "Dron2", "class_dron2.html", null ],
      [ "Dron3", "class_dron3.html", null ],
      [ "Graniastoslup6", "class_graniastoslup6.html", null ],
      [ "Plaskowyz", "class_plaskowyz.html", null ],
      [ "Prostopadloscian", "class_prostopadloscian.html", null ],
      [ "Wzgorze", "class_wzgorze.html", null ]
    ] ],
    [ "Wektor< ROZMIAR >", "class_wektor.html", null ],
    [ "Wektor< 3 >", "class_wektor.html", null ]
];