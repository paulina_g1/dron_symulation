#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <thread>
#include "Wektor.hh"
#include "Macierz.hh"
#include "Czesci_dron.hh"
#include "Drony.hh"
#include "Elem_krajobrazu.hh"
#include "Scena.hh"

void wyswietl_menu(){
    std::cout << "Dostepne opcje: " << std::endl;
    std::cout << "**************************************" << std::endl;
    std::cout << "a - przelacz aktywnego drona" << std::endl;
    std::cout << "l - lec dronem" << std::endl;
    std::cout << "p - pokaz wszystkie drony" << std::endl;
    std::cout << "d - dodaj nowego drona" << std::endl;
    std::cout << "u - usun drona" << std::endl;
    std::cout << "w - pokaz wszystkie elem. krajobrazu" << std::endl;
    std::cout << "n - dodaj nowy elem. krajobrazu" << std::endl;
    std::cout << "s - usun elem. krajobrazu" << std::endl;
    std::cout << "m - wyswietl menu" << std::endl;
    std::cout << "k - zakoncz program  " << std::endl;
}

int main(){ 

char wybor;
drawNS::Draw3DAPI * rysownik = new drawNS::APIGnuPlot3D(-20,20,-20,20,-5,20,-1);
Scena nowa_scena;
nowa_scena.rysuj_wszystkie(rysownik);

wyswietl_menu();
while (wybor!= 'k'){
    std::cout << "Twoj wybor: (m - menu) > ";
    std::cin >> wybor;
    do{
    std::cin.clear();
    std::cin.ignore(10000, '\n');
    switch(wybor){
    case 'a' : //przelacz drona
    nowa_scena.wybierz_dron();
    break;
    case 'l' : //lec
    nowa_scena.animacja(rysownik);
    break;
    case 'p' : //pokaz drony
    nowa_scena.pokaz_drony();
    break;
    case 'd' : //dodaj drona
    nowa_scena.usun_wszystkie(rysownik);
    nowa_scena.dodaj_dron();
    nowa_scena.rysuj_wszystkie(rysownik);
    break;
    case 'u' : //usun drona
    nowa_scena.usun_wszystkie(rysownik);
    nowa_scena.usun_dron();
    nowa_scena.rysuj_wszystkie(rysownik);
    break;
    case 'w' : //pokaz elem. krajobrazu
    nowa_scena.pokaz_elem_kraj();
    break;
    case 'n' : //dodaj elem. krajobrazu
    nowa_scena.usun_wszystkie(rysownik);
    nowa_scena.dodaj_elem_kraj();
    nowa_scena.rysuj_wszystkie(rysownik);
    break;
    case 's' : //usun elem. krajobrazu
    nowa_scena.usun_wszystkie(rysownik);
    nowa_scena.usun_elem_kraj();
    nowa_scena.rysuj_wszystkie(rysownik);
    break;
    case 'm' :  //wyswielt menu
        wyswietl_menu();
        break;
    case 'k' : //zakoncz program
        break;
    default : 
        std::cerr << "Brak takiej opcji!" << std::endl;
        std::cout << "Sprobuj jeszcze raz: " << std::endl; break;
    }
    }while(std::cin.fail());
}


}

