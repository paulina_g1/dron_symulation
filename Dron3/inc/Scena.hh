#ifndef SCENA_HH
#define SCENA_HH

/*!
* \file
* \brief Definicje klasy Scena
*
* Plik zawiera definicję klasy Scena
*/

#include "Czesci_dron.hh"
#include "Elem_krajobrazu.hh"
#include "Drony.hh"
#include "Interfejsy.hh"
#include <iostream>
#include <array>
#include <algorithm>

/*!
* \brief Modeluje pojęcie Sceny
*/
class Scena {
/*!
* \brief kontener z elementami sceny które mają być narysowane
*/
std::vector<std::shared_ptr<Interfejs_rysowanie>> Do_narysowania;
/*!
* \brief kontener z dronami znajdującymi się na scenie
*/
std::vector<std::shared_ptr<Interfejs_Dron>>Drony;
/*!
* \brief kontener z elementami krajobrazu i dronami znajdującyhmik się na scenie
*/
std::vector<std::shared_ptr<Interfejs_elem_kraj>>Elementy_krajobrazu;
/*!
* \brief wskaźnik na aktualny aktywny dron
*/
std::shared_ptr<Interfejs_Dron> Aktualny_dron;
public:
/*!
* \brief Rysuje wszystkie elementy z kontenera Do_narysowania
* \param[in]    rysownik    wskaźnik potrzebny do rysowania za pomocą Dr3D_gnuplot_api
*/
void rysuj_wszystkie(drawNS::Draw3DAPI* rysownik);
/*!
* \brief Usuwa wszystkie rysunki elementów z kontenera Do_narysowania
* \param[in]    rysownik    wskaźnik potrzebny do rysowania za pomocą Dr3D_gnuplot_api
*/
void usun_wszystkie(drawNS::Draw3DAPI* rysownik);
/*!
* \brief Animuje przelot drona
* \param[in]    rysownik    wskaźnik potrzebny do rysowania za pomocą Dr3D_gnuplot_api
*/
void animacja(drawNS::Draw3DAPI* rysownik);
/*!
* \brief sprawdza czy można wykonać lądowanie aktywnym dronem
* \param[out]    wysokość    wysokość na której można lądować
*/
bool czy_mozna_ladowac(double & wysokosc);
/*!
* \brief dodaje nowy element do kolekcji Elementy_krajobrazu
*/
void dodaj_elem_kraj();
/*!
* \brief usuwa wybrany przez użytkownika element z kolekcji Elementy_krajobrazu
*/
void usun_elem_kraj();
/*!
* \brief wypisuje wszystkie elementy znajdujące się obecnie na scenie
*/
void pokaz_elem_kraj();
/*!
* \brief dodaje nowy dron do kolejkcji Drony i do kolekcji Elementy_krajobrazu
*/
void dodaj_dron();
/*!
* \brief usuwa wybrany przez użytkownika dron z kolekcji Elementy_krajobrazu i kolejkcji Drony
*/
void usun_dron();
/*!
* \brief wypisuje wszystkie dronyu znajdujące się obecnie na scenie
*/
void pokaz_drony();
/*!
* \brief umożliwia użytkownikowi zmianę aktywnego drona na inny z znajdujążch się obecnie na scenie
*/
void wybierz_dron();
/*!
* \brief konstruktor
* Ustawia domyślnie na scenie: powierzchnie, 3 elementy krajobrazu, 3 drony i ustawia aktywny dron
*/
Scena(){ 
    Do_narysowania.push_back(std::shared_ptr<Interfejs_rysowanie>(new Powierzchnia(-0.5)));
    Elementy_krajobrazu.push_back(std::shared_ptr<Interfejs_elem_kraj>(new Plaskowyz(Wektor<3>({10,10,0}),3,9)));
    Do_narysowania.push_back(std::dynamic_pointer_cast<Interfejs_rysowanie>(Elementy_krajobrazu.back()));
    Elementy_krajobrazu.push_back(std::shared_ptr<Interfejs_elem_kraj>(new Wzgorze(Wektor<3>({-10,10,0}),3,9)));
    Do_narysowania.push_back(std::dynamic_pointer_cast<Interfejs_rysowanie>(Elementy_krajobrazu.back()));
    Elementy_krajobrazu.push_back(std::shared_ptr<Interfejs_elem_kraj>(new Plaskowyz_prost(Wektor<3>({10,-10,0}),3,9)));
    Do_narysowania.push_back(std::dynamic_pointer_cast<Interfejs_rysowanie>(Elementy_krajobrazu.back()));
    Drony.push_back(std::shared_ptr<Interfejs_Dron>(new Dron(Wektor<3>({-15,-15,0}),Macierz_Rot<3>())));
    Elementy_krajobrazu.push_back(std::dynamic_pointer_cast<Interfejs_elem_kraj>(Drony.back()));
    Do_narysowania.push_back(std::dynamic_pointer_cast<Interfejs_rysowanie>(Drony.back()));
    Drony.push_back(std::shared_ptr<Interfejs_Dron>(new Dron2(Wektor<3>({-5,-5,0}),Macierz_Rot<3>())));
    Elementy_krajobrazu.push_back(std::dynamic_pointer_cast<Interfejs_elem_kraj>(Drony.back()));
    Do_narysowania.push_back(std::dynamic_pointer_cast<Interfejs_rysowanie>(Drony.back()));
    Drony.push_back(std::shared_ptr<Interfejs_Dron>(new Dron3(Wektor<3>({15,0,0}),Macierz_Rot<3>(90,Wektor<3>({0,0,1})))));
    Elementy_krajobrazu.push_back(std::dynamic_pointer_cast<Interfejs_elem_kraj>(Drony.back()));
    Do_narysowania.push_back(std::dynamic_pointer_cast<Interfejs_rysowanie>(Drony.back()));
    Aktualny_dron = Drony.front();
    };
};

#endif