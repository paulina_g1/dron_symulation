#ifndef MACIERZ_HH
#define MACIERZ_HH

/*!
* \file
* \brief Definicja klasy Macierz
*
* Plik zawiera definicję klasy Macierz
* oraz przeciążenia operatora <<
*/

#include"Wektor.hh"
#include <iostream>
#include <array>

/*!
* \brief Modeluje pojęcie Macierz obrotu
*
* Klasa modeluje pojęcie macierzy o zadanym wymiarze (2 lub 3)
*/
template <int ROZMIAR> 
class Macierz_Rot {
/*!
* \brief Tablica wektorów o zadanym wymiarze - wiersze macierzy rotacji
*/
  std::array<Wektor<ROZMIAR>,ROZMIAR> wiersz;
  public:
/*!
* \brief Realizuje transpozycje macierzy
* \return Transponowaną macierz
*/
  Macierz_Rot<ROZMIAR> transpozycja()const;  
/*!
* \brief Realizuje mnozenie macierzy razy wektor (nie na odwrot)
* \param[in]    skl2    wektor razy ktory mnozymy macierz
* \return Wynik mnozenia macierzy razy wektor (tez wektor) 
*/
  Wektor<ROZMIAR> operator*(const Wektor<ROZMIAR> & skl2) const;
/*!
* \brief Realizuje mnozenie dwoch macierzy  
* \param[in]    skl2    druga macierz
* \return Wynik mnozenia dwuch macierzy (tez macierz) 
*/
  Macierz_Rot<ROZMIAR> operator*(const Macierz_Rot<ROZMIAR> & skl2) const;
/*!
* \brief Konstruktor, tworzy macierz identycznościową
*/
  Macierz_Rot();
/*!
* \brief Konstruktor
* \param kat_stopnie kąt o jaki ma obracać macierz
* \param os os wokół której ma obracać
*/
  Macierz_Rot(double kat_stopnie, Wektor<ROZMIAR> os);
/*!
* \brief  geter, udostępnia poszczególne wiersze macierzy
* \param i numer wiersza do zwrócenia
* \return zadany wiersz
*/
  const Wektor<ROZMIAR> & operator[](int i) const{
    if(i<0||i>ROZMIAR-1){ std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wiersz[i];};
};
/*!
* Wypisuje macierz na strumien wyjsciowy	  
* \param[in]    strm    strumien
* \param[in]   mac  wyswietlana macierz 
* \return Strumien z zapisem macierzy 
*/
template <int ROZMIAR> 
std::ostream& operator << (std::ostream &Strm, const Macierz_Rot<ROZMIAR> &Mac);

#endif
