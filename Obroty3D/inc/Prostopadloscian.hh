#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH

#include "Wektor.hh"
#include "Macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include <iostream>
#include <array>



class Prostopadloscian {
  std::array<Wektor<3>,8> wierzcholek;
  int id;
  bool czy_bok_to_prostokat(Wektor<3> wsp0, Wektor<3> wsp1, Wektor<3> wsp2, Wektor<3> wsp3) const;
public:
  void obrot(Macierz_Rot<3> mac_rot);
  void przesuniecie (Wektor<3> Wek);
  bool czyProstopadloscian() const;
  void rysuj(drawNS::Draw3DAPI* rysownik);
  void usun_rys(drawNS::Draw3DAPI* rysownik) const;
  //konstruktory:
  Prostopadloscian (std::array<Wektor<3>,8> w);
  //get:
  const Wektor<3> & operator[] (int i) const{
    if(i<0||i>8){std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wierzcholek[i];}

};

std::ostream& operator << ( std::ostream &Strm,const Prostopadloscian & Pr);
drawNS::Point3D konwertuj(Wektor<3> W);

#endif
