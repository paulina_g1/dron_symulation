#ifndef WEKTOR_HH
#define WEKTOR_HH

#include <iostream>
#include <array>
#include <cmath>

/*modeluje pojecie wektora*/

template <int ROZMIAR>
class Wektor {
  std::array<double,ROZMIAR> wsp;
  public:
  Wektor<ROZMIAR> operator+(const Wektor<ROZMIAR> & skl2) const;
  Wektor<ROZMIAR> operator-(const Wektor<ROZMIAR> & skl2) const;
  Wektor<ROZMIAR> operator*(double skl2) const;
  double operator*(const Wektor<ROZMIAR> & skl2) const; //skalarne
  double dlugosc() const;
  // konstruktory:
  Wektor() = default;
  Wektor(std::array<double,ROZMIAR> arg): wsp(arg) {};
  //get:
  const double & operator[] (int i) const{
    if(i<0||i>ROZMIAR-1){std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wsp[i];}
  //set:
  double & operator[](int i){
    if(i<0||i>ROZMIAR-1){std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wsp[i];}
};

template <int ROZMIAR>
std::istream& operator >> (std::istream &strm, Wektor<ROZMIAR> &wek);

template <int ROZMIAR>
std::ostream& operator << (std::ostream &strm, const Wektor<ROZMIAR> &wek);

#endif
